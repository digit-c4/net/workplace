$version: "2"
namespace eu.europa.ec.snet.DCA.PortChannelCreate

@documentation("A representation of a complete object to create a Port-Channel.")
structure PortChannelCreate {
    @documentation("The port channel description.")
    description: String,

    @required
    @uniqueItems
    @maxItems(8)
    @documentation("The list of interfaces in the port channel.")
    interfaces: InterfaceList,

    @enum(["sync", "async"])
    @documentation("The job in sync or async mode.")
    jobMode: String,

    @required
    @enum(["active", "on"])
    @documentation("The etherchannel mode: active -> LACP, on -> PAgP. If you do not know, this should be active -> LACP mode for almost all configurations.")
    poMode: String,

    @required
    @minLength(1)
    @documentation("The job reference.")
    reference: String
}

@documentation("A list of interfaces.")
list InterfaceList {
    member: String
}
