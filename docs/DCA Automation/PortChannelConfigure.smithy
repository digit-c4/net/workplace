
$version: "2"
namespace eu.europa.ec.snet.DCA.PortChannelConfigure

@documentation("A representation of a complete object to configure a port-channel.")
structure PortChannelConfigure {
    @required
    @enum(["up", "down"])
    @documentation("The administrative state of the port-channel.")
    adminState: String,

    @required
    @enum(["auto", "full", "half"])
    @documentation("The duplex of the port-channel.")
    duplex: String,

    @enum(["sync", "async"])
    @documentation("The job in sync or async mode.")
    jobMode: String,

    @required
    @minLength(1)
    @documentation("The job reference.")
    reference: String,

    @required
    @enum(["auto", "10M", "100M", "1G", "10G"])
    @documentation("The normalized speed of the port-channel.")
    speed: String,

    @required
    @uniqueItems
    @documentation("The list of the trunked/tagged VLANs.")
    tagged: TaggedVlanList,

    @required
    @range(min: 1, max: 4095)
    @documentation("The native VLAN.")
    vlan: Integer
}

@documentation("A list of trunked/tagged VLANs.")
list TaggedVlanList {
    member: TaggedVlan
}

@documentation("A single trunked/tagged VLAN.")
integer TaggedVlan {
    @range(min: 1, max: 4095)
}