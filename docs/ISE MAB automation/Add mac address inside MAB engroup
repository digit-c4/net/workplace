namespace netbox.automation

service NetboxMacAddressService {
    version: "1.0.0"
    operations: [ConfigureMacAddress, TriggerAWXPlaybook, ScheduleMacRemoval]
}

structure MacAddress {
    @required
    mac: String
    vlan: String
    isTemporary: Boolean
    endDate: Timestamp
}

operation ConfigureMacAddress {
    input: ConfigureMacAddressInput
    output: ConfigureMacAddressOutput
}

structure ConfigureMacAddressInput {
    @required
    macAddress: MacAddress
}

structure ConfigureMacAddressOutput {
    macAddressId: String
    status: String
}

operation TriggerAWXPlaybook {
    input: TriggerAWXPlaybookInput
    output: TriggerAWXPlaybookOutput
}

structure TriggerAWXPlaybookInput {
    @required
    macAddressId: String
    @required
    encodedMacAddress: String
}

structure TriggerAWXPlaybookOutput {
    status: String
}

operation ScheduleMacRemoval {
    input: ScheduleMacRemovalInput
    output: ScheduleMacRemovalOutput
}

structure ScheduleMacRemovalInput {
    @required
    macAddressId: String
    @required
    removalDate: Timestamp
}

structure ScheduleMacRemovalOutput {
    status: String
}

@trait(selector: "*", extends: [documentation], type: "string")
metadata {
    "documentation": """
    This service allows administrators to manage MAC addresses in Netbox,
    trigger AWX playbooks for ISE endpoint groups, and schedule MAC address removal 
    for temporary addresses.

    - Connects to Netbox using the MAC address tool.
    - Configures a MAC address, assigning it to the appropriate VLAN.
    - If temporary, sets an end date for automatic removal.
    - Triggers AWX playbooks to sync MAC addresses with ISE endpoint groups.
    - Supports scheduling for automatic removal from ISE and Netbox.

    ### Playbook Integrations

    **add_endpoint:** Adds a new endpoint to Cisco ISE and NetBox, handling updates for existing endpoints, creating journal entries, and setting up scheduling for automatic removal.

    **Remove_endpoint:** Deletes an endpoint from both Cisco ISE and NetBox once the scheduled end date is reached, also updating the NetBox journal with a job entry upon deletion.
    """
}