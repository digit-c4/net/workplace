namespace netbox.netbox

// Define the service for managing network distribution switch
service NetBoxService {
    version: "1.0.0"
    operations: [ImportDSNetBox]
}

// Define the device resource
resource Device {
    string hostname
    string ip
    string mgmt_vlan
    string site
}

// Define the operations for the service
operation ImportDSNetBox {
    input: ImportDSNetBoxInput
    output: ImportDSNetBoxOutput
}

structure ImportDSNetBoxInput {
    @required
    string hostname

    @required
    string ip

    @required
    string mgmt_vlan

    @required
    string site
}

// Additional documentation on the operation

// Operation ImportDSNetBox
// The ImportDSNetBox operation allows the user to create import a distributione switch in NetBox.
// All fields are mandatory and must be provided in the request body. The operation
// will return a response indicating whether the creation was successful.

// Example Request for ImportDSNetBox:
// {
//     "hostname": "device_name",
//     "ip": "1.2.4.5/24",
//     "mgmt_vlan": "Vlan123",
//     "site": "site_name"
// }

// Example Response (Failure - Missing Fields):
// {
//     "message": "Not all the parameters were provided. Stopping...",
//     "success": false
// }
