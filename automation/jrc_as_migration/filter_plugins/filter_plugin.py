from ciscoconfparse import CiscoConfParse
import re
from ansible.errors import AnsibleFilterError
import pynetbox
import sys
import os

# Adjust sys.path to include the root of the project
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

#Import all the interface packages/__init__.py --> __all__; if the method it`s not in all it will not work.
from packages import *

#Combine the parameters so that the profiles will result.
def build_final_parse(config, base_hostname, oob_ip, nac_vlans, ap_vlans, site):
    """
    Parse the Cisco running-config and return structured data for the device and its interfaces.
    The hostname is modified to include the stack number if available.
    """
    
    try:
        # Parse the configuration using CiscoConfParse
        parse = CiscoConfParse(config.splitlines(), syntax='ios')

        # Get the parsed data from the individual functions
        excluded_uplinks = get_excluded_interfaces_uplinks(parse)
        excluded_downlink = get_excluded_interfaces_downlinks(parse)
        interface_types = get_interface_type(parse)
        descriptions = parse_descriptions(parse)
        mode_mapping, vlan_mapping, native_vlan_mapping, voice_vlan_mapping = get_vlan_and_mode(parse)
        dot1x_interface = get_dot1x_interface(parse)
        #ip_interfaces = get_ip_interface(parse)
        speed_mapping = get_speed(parse)
        duplex_mapping = get_duplex(parse)
        enabled_status_mapping = get_enabled_status(parse)
        port_security_mapping = get_port_security(parse)
        mac_sticky_mapping = get_mac_sticky(parse)
        #mac_address_mapping = get_mac_address(parse)  
        mac_sticky_maximum_mapping = get_mac_sticky_maximum(parse)
        channel_group_mode_mapping = get_channel_group_mode(parse)
        port_security_aging_mapping = get_port_security_aging(parse)
        pid = get_pid(parse)
        ap_interfaces = get_ap_interfaces(parse)
        supervisor_info = get_supervisor_4500(parse)
        stack_info = get_stack(config)
        #base_hostname = get_hostname(parse) # to be get from netbox
        system_sn = extract_system_serial_numbers(parse)
        line_cards_supervisors = get_line_cards_supervisors(parse)
        power_supply_info = extract_power_supply_info(parse)
        fan_tray_info = extract_fan_tray(parse)
        uplink_module = get_uplink_module(parse)
        interface_sticky = get_sticky(parse)
        portfast_status = get_portfast_status(parse)

        # Initialize the final device data dictionary
        device_data = {}

        # Check if there's only one switch (not a stack)
        is_stack = len(stack_info) > 1

        # Start Parsing the interfaces with the parameters and profiles
        for interface_name in interface_types:

            # Ignore the SVIs except the OOB one
            if "Vlan" in interface_name:
                continue
            
            stack_role = None
            # Extract the stack member from the interface name first digit e.g. 1/x/x
            match = re.search(r'(\w+)(\d+)', interface_name)
            if match:
                stack_member = match.group(2)
            else:
                stack_member = "1" # If no match fall to 1 by default

            # Build the hostname with the stack member if available
            if is_stack and stack_member and stack_member in stack_info:
                final_hostname = f"{base_hostname}-{stack_member}"
                stack_role = stack_info.get(stack_member, {}).get('role', None)
            elif is_stack:
                final_hostname = f"{base_hostname}-{min(stack_info)}"
                stack_role = stack_info.get(min(stack_info), {}).get('role', None)
            else:
                final_hostname = base_hostname
                stack_role = None

            # Check if the interface is part of a port-channel and set port-channel mode and number
            port_channel_mode = ""
            port_channel_number = None
            channel_group_info = channel_group_mode_mapping.get(interface_name, "")
            if channel_group_info:
                channel_group_mode, port_channel_id = channel_group_info
                port_channel_mode = channel_group_mode
                port_channel_number = port_channel_id

            # Get the mode of the interface
            mode = mode_mapping.get(interface_name, "")
            if mode == "trunk": # Change the trunk mode to the accepted value "tagged"
               mode = "tagged"
            
            # Prepare the interface data
            interface_data = {
                "name": interface_name,
                "description": descriptions.get(interface_name, ""),
                "type": interface_types.get(interface_name, ""),
                "mode": mode,
                "speed": speed_mapping.get(interface_name, ""),
                "duplex": duplex_mapping.get(interface_name, ""),
                "enabled": enabled_status_mapping.get(interface_name, True),
                "custom_fields": {
                    "port_channel_mode": port_channel_mode,
                    "speed_Mbps": speed_mapping.get(interface_name, ""),
                    "portfast_status": portfast_status.get(interface_name, ""),
                    "PortProfile": ""
                }
            }
            
            # Include port_channel_number if present
            if port_channel_number:
                interface_data["custom_fields"]["port_channel_number"] = port_channel_number

            # Mark UPLINK and DOWNLINK interfaces with specific tags
            if interface_name in excluded_uplinks:
                interface_data["custom_fields"]["PortProfile"] = "profile_uplink" 
            elif interface_name in excluded_downlink:
                interface_data["custom_fields"]["PortProfile"] = "profile_inter_switch_link" 

            #Update the interface_data with tagged or untagged VLANs
            catalyst_4500_models = ["WS-C4506","WS-C4507R","WS-C4510R","WS-C4503"]
            catalyst_3750_models = ["WS-C3750G","WS-C3750X"]
            sup_models = ["Supervisor II"]
            if mode_mapping.get(interface_name) == 'access':
                vlan_value = vlan_mapping.get(interface_name)
                voice_vlans = voice_vlan_mapping.get(interface_name, [])
                interface_data["untagged_vlan"] = vlan_value
                interface_data["custom_fields"]["access_vlan"] = vlan_value
                if voice_vlans:
                    interface_data["custom_fields"]["voice_vlan"] = voice_vlans
            elif mode_mapping.get(interface_name) == 'trunk':
                tagged_vlans_for_interface = vlan_mapping.get(interface_name)
                if tagged_vlans_for_interface:
                    tagged_vlans_str = ','.join(map(str, tagged_vlans_for_interface)) # Convert the lists of the VLANs in string only for Custom Fields
                    if interface_data["custom_fields"]["PortProfile"] != "profile_uplink" and interface_data["custom_fields"]["PortProfile"] != "profile_inter_switch_link":
                        # If the profile is defined, not empty, and not "profile_uplink", assign tagged VLANs
                        interface_data["tagged_vlans"] = tagged_vlans_for_interface
                    else:
                        ## If the profile is "profile_uplink", leave tagged VLANs empty
                        interface_data["tagged_vlans"] = ""
                    interface_data["custom_fields"]["allowed_vlans_trunk"] = tagged_vlans_str 
                if interface_name in native_vlan_mapping:
                    native_vlan_value = native_vlan_mapping[interface_name]
                    interface_data["untagged_vlan"] = native_vlan_value
                    interface_data["custom_fields"]["native_vlan"] = native_vlan_value
                #Adding encapsulation dot1q for 4500 SUPII and 3750
                #Use case for c4k and c3750 where the trunk encap should be true
                if pid in catalyst_4500_models and sup_models == supervisor_info or pid in catalyst_3750_models:
                    interface_data['custom_fields']["trunk_encap"] = True
                else:
                    interface_data['custom_fields'].pop("trunk_encap",None)
            
            # Ensure Port Profile logic for APs, NAC, PortSec, and other configurations
            if (mode_mapping.get(interface_name) in ["access", "trunk"]) and (interface_name not in excluded_uplinks and interface_name not in excluded_downlink):
                port_security = port_security_mapping.get(interface_name, "")
                mac_sticky = mac_sticky_mapping.get(interface_name, "")
                vlans_security = vlan_mapping.get(interface_name, [])
                security_aging = port_security_aging_mapping.get(interface_name, "")
                voice_vlans = voice_vlan_mapping.get(interface_name, [])
                dot1x = dot1x_interface.get(interface_name, False)  # Assume False if dot1x is not configured
                sticky = interface_sticky.get(interface_name, False) #Assume False if mac-sticky si not configured
                portsecmax = mac_sticky_maximum_mapping.get(interface_name)

                # Convert vlans_security and voice_vlans to lists if they are not already
                if not isinstance(vlans_security, list):
                    vlans_security = [vlans_security]
                if not isinstance(voice_vlans, list):
                    voice_vlans = [voice_vlans]

                # Log interface parameters for debugging
                #print(f"Interface: {interface_name}, Mode: {mode_mapping.get(interface_name)}, NAC VLANs: {nac_vlans}, Voice VLANs: {voice_vlans}, Security VLANs: {vlans_security}, Dot1x: {dot1x}")

                # Additional fields for port security aging and MAC sticky address
                if interface_name not in ap_interfaces and any(vlan not in nac_vlans for vlan in vlans_security):
                    interface_data["custom_fields"]["port_security_aging"] = security_aging
                    #print(f"Set port_security_aging for {interface_name} due to non-NAC VLANs present")

                if mac_sticky and any(vlan not in nac_vlans for vlan in vlans_security):
                    interface_data["custom_fields"]["mac_sticky_address"] = mac_sticky
                    #print(f"Set mac_sticky_address for {interface_name} due to mac_sticky and non-NAC VLANs present")

                if portsecmax and any(vlan not in nac_vlans for vlan in vlans_security):
                    interface_data["custom_fields"]["portsecmax"] = portsecmax

                # 1. If both NAC and Voice VLANs are attached, assign profile_voice_nac
                if any(vlan in nac_vlans for vlan in vlans_security) and interface_name in voice_vlan_mapping:
                    interface_data["custom_fields"]["PortProfile"] = "profile_voice_nac"
                    #print(f"Assigned profile_voice_nac for {interface_name} due to both NAC and Voice VLANs")

                # 2. If only NAC VLANs are present, assign profile_nac
                elif (
                    any(vlan in nac_vlans for vlan in vlans_security)
                    and interface_name not in ap_interfaces
                    #and interface_name not in excluded_uplinks
                    #and interface_name not in excluded_downlink
                ):
                    interface_data["custom_fields"]["PortProfile"] = "profile_nac"

                # 3. If voice VLAN is present and dot1x is enabled, assign profile_voice_nac
                elif interface_name in voice_vlan_mapping and dot1x and interface_name not in ap_interfaces:
                    interface_data["custom_fields"]["PortProfile"] = "profile_voice_nac"
                    #print(f"Assigned profile_voice_nac for {interface_name} due to Voice VLAN and dot1x enabled")
                
                # 4. If only Voice VLAN is present, and dot1x is not enabled, assign profile_access_user_voice
                elif interface_name in voice_vlan_mapping and not dot1x and interface_name not in ap_interfaces:
                    interface_data["custom_fields"]["PortProfile"] = "profile_access_user_voice"
                    #print(f"Assigned profile_access_user_voice for {interface_name} due to Voice VLAN without dot1x")

                # 5. AP profile based on interface check and NAC absence
                elif interface_name in ap_interfaces and not any(vlan in nac_vlans for vlan in vlans_security):
                    interface_data["custom_fields"]["PortProfile"] = "profile_ap"
                    #print(f"Assigned profile_ap for {interface_name} as it is in AP interfaces and no NAC VLAN")
                
                # 5.1 AP profile based on interface check and NAC absence for karlsruhe
                elif not any(vlan in nac_vlans for vlan in vlans_security) and (len(ap_vlans) > 0 and any(vlan in ap_vlans for vlan in vlans_security) and site == 'karlsruhe'):
                    interface_data["custom_fields"]["PortProfile"] = "profile_ap"
                    #print(f"Assigned profile_ap for {interface_name} as it is in AP interfaces and no NAC VLAN")

                # 6. Port security without sticky MACs TO DO if only mac sticky assign mac sticky 
                elif port_security and not sticky and not any(vlan in nac_vlans for vlan in vlans_security) and interface_name not in voice_vlan_mapping and interface_name not in ap_interfaces:
                    interface_data["custom_fields"]["PortProfile"] = "profile_portsec_no_sticky"
                    #print(f"Assigned profile_portsec_no_sticky for {interface_name}")

                # 7. Port security with sticky MACs
                elif port_security and sticky and not any(vlan in nac_vlans for vlan in vlans_security) and interface_name not in voice_vlan_mapping and interface_name not in ap_interfaces:
                    interface_data["custom_fields"]["PortProfile"] = "profile_portsec_sticky"
                    #print(f"Assigned profile_portsec_sticky for {interface_name}")

                # 8. Default profile for other access user cases
                else:
                    interface_data["custom_fields"]["PortProfile"] = "profile_access_user"
                    #print(f"Assigned profile_access_user for {interface_name} as default")

            # Profile for trunk mode AP interfaces
            elif mode_mapping.get(interface_name) == "trunk" and interface_name in ap_interfaces:
                interface_data["custom_fields"]["PortProfile"] = "profile_ap"
                #print(f"Assigned profile_ap for {interface_name} in trunk mode as it is an AP interface")

            # Profile when mode is neither trunk nor access
            elif mode_mapping.get(interface_name) not in ["trunk", "access"]:
                interface_data["custom_fields"]["PortProfile"] = "profile_no_profile"
                #print(f"Assigned profile_no_profile for {interface_name} as it is neither trunk nor access")

            # Organize the device data under the appropriate hostname
            if final_hostname not in device_data:
                device_data[final_hostname] = {
                    "supervisor": supervisor_info,
                    "interfaces": [],
                    "oob_ip": oob_ip,
                    "model": pid,
                    "serial_numbers": [],
                    "line_cards_supervisors": line_cards_supervisors,
                    "stack_name": None,
                    "stack_role": None,
                    "fan_tray_info": None,
                    "power_supply": power_supply_info,
                    "is_stack": is_stack,
                    "uplink_module": uplink_module
                }

            device_data[final_hostname]["interfaces"].append(interface_data)

            # Assign the serial number per switch for stack/non-stack. 
            for system_sn_entry in system_sn:
                if is_stack:#stack
                    hostname = system_sn_entry["hostname"]
                    if hostname in device_data:
                        device_data[hostname].update({
                            "stack_name": base_hostname,
                            "stack_role": stack_role
                        })
                        serial_data = {
                            "hostname": hostname,
                            "system_serial_number": system_sn_entry["system_serial_number"],
                            "version": system_sn_entry["version"],
                        }
                        if serial_data not in device_data[hostname]["serial_numbers"]:
                            device_data[hostname]["serial_numbers"].append(serial_data)
                else:#non-stack
                    device_data[final_hostname].update({
                        "fan_tray_info": fan_tray_info
                    })
                    serial_data = {
                        "hostname": final_hostname,
                        "system_serial_number": system_sn_entry["system_serial_number"],
                        "version": system_sn_entry["version"]
                       }
                    if serial_data not in device_data[final_hostname]["serial_numbers"]:
                       device_data[final_hostname]["serial_numbers"].append(serial_data)

        return device_data

    except Exception as e:
        raise AnsibleFilterError(f"Error parsing Cisco config: {str(e)}")

# Ansible filter plugin
class FilterModule(object):
    def filters(parse):
        return {
            'parse_cisco_config': build_final_parse #final filter which u call in ansible. f works
        }
