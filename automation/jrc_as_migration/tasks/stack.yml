- name: Stack - Block to handle stack case
  block:

    - name: Stack - Get stack devices with their interfaces
      set_fact:
        stack_device_interfaces: >-
          {%- set devices_with_interfaces = [] -%}
          {%- for device in device_names_stack -%}
            {%- for interface in device['interfaces'] -%}
              {%- set device_interface = {
                    "device_name": device.device_name,
                    "device_type": device.device_type,
                    "stack_name": device.stack_name,
                    "interface_name": interface.name,
                    "interface_description": interface.description,
                    "interface_type": interface.type,
                    "interface_mode": interface.mode,
                    "interface_speed": interface.speed,
                    "interface_duplex": interface.duplex,
                    "interface_enabled": interface.enabled,
                    "untagged_vlan": interface.untagged_vlan | default(omit),
                    "tagged_vlans": interface.tagged_vlans | default(omit),
                    "uplink": interface.uplink | default(False),
                    "downlink": interface.downlink | default(False),
                    "custom_fields": interface.custom_fields
              } -%}
              {%- set _ = devices_with_interfaces.append(device_interface) -%}
            {%- endfor -%}
          {%- endfor -%}
          {{ devices_with_interfaces }}

    - name: Extract the port-channel interfaces
      set_fact:
        port_channel_interfaces: >-
          {%- set devices_with_interfaces = [] -%}
            {%- for device in device_names_stack -%}
              {%- for interface in device['interfaces'] -%}
                {%- if interface['custom_fields']['port_channel_number'] is defined and interface['custom_fields']['port_channel_number'] is not none and interface['custom_fields']['port_channel_mode'] is defined and interface['custom_fields']['port_channel_mode'] is not none -%}
                  {%- set device_interface = {
                        "device_name": device.device_name,
                        "device_type": device.device_type,
                        "stack_name": device.stack_name,
                        "interface_name": interface.name,
                        "interface_description": interface.description,
                        "interface_type": interface.type,
                        "interface_mode": interface.mode,
                        "interface_speed": interface.speed,
                        "interface_duplex": interface.duplex,
                        "interface_enabled": interface.enabled,
                        "untagged_vlan": interface.untagged_vlan | default(omit),
                        "tagged_vlans": interface.tagged_vlans | default(omit),
                        "uplink": interface.uplink | default(False),
                        "downlink": interface.downlink | default(False),
                        "custom_fields": interface.custom_fields
                  } -%}
                  {%- set _ = devices_with_interfaces.append(device_interface) -%}
                {%- endif -%}
              {%- endfor -%}
            {%- endfor -%}
            {{ devices_with_interfaces }}
    
    - name: Stack - Create device types within NetBox with only required information
      netbox.netbox.netbox_device_type:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        data:
          model: "{{ item }}"
          manufacturer: "Cisco"
        state: present
      loop: "{{ device_names_stack | map(attribute='device_type') | unique }}"
      loop_control:
        label: "Creating the device type {{ item }}"
        
    - name: Stack - Determine master device for each stack
      set_fact:
        stack_masters: >-
          {%- set masters = [] -%}
          {%- for stack_name in device_names_stack | map(attribute='stack_name') | unique -%}
            {%- set devices_in_stack = device_names_stack | selectattr('stack_name', 'equalto', stack_name) | sort(attribute='vc_priority', reverse=True) -%}
            {%- set _ = masters.append({
                  "stack_name": stack_name,
                  "master_device": devices_in_stack[0]['device_name']
                }) -%}
          {%- endfor -%}
          {{ masters }}
    
    - name: Stack - Create device within NetBox with only required information
      ansible.builtin.include_role:
        name: netbox_device
        tasks_from: stack.yml
    
    - name: Stack -  Create Power Supply
      ansible.builtin.include_role:
        name: netbox_inventory_item
        tasks_from: power_supply.yml
    
    - name: Non-Stack -  Create Uplink Module
      ansible.builtin.include_role:
        name: netbox_inventory_item
        tasks_from: uplink.yml

    - name: Stack - Create virtual chassis within NetBox with only required information
      netbox.netbox.netbox_virtual_chassis:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        state: present
        data:
          name: "{{ device_names_stack | map(attribute='stack_name') | unique | first }}"
          master: "{{ stack_masters | map(attribute='master_device') | first }}"  # Device with highest priority

    - name: Stack - Update the device with the VC information
      netbox.netbox.netbox_device:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        state: present
        data:
          name: "{{ item.device_name }}"
          vc_position: "{{ item.vc_position }}"
          vc_priority: "{{ item.vc_priority }}"
          virtual_chassis: "{{ item.stack_name }}"
      loop: "{{ device_names_stack }}"
      loop_control:
        label: "Creating device {{ item.device_name }}"
    
    - name: Stack - Create interface for OOB
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        state: present
        data:
          device: "{{ stack_masters | map(attribute='master_device') | first }}"
          name: "Vlan4002"
          type: "Virtual"
        state: present
        
    - name: Stack - Map IP address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        data:
          address: "{{ device_names_stack[0]['oob_ip'] }}"
          assigned_object:
            device: "{{ stack_masters | map(attribute='master_device') | first }}"
            name: "Vlan4002"
        state: present
        query_params: [ "address" ]
    
    - name: Stack - Update device with the OOB
      netbox.netbox.netbox_device:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        state: present
        data:
          name: "{{  stack_masters | map(attribute='master_device') | first  }}"
          oob_ip: "{{ device_names_stack[0]['oob_ip'] }}"
        
    - name: Stack - Create interfaces for each stack device
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ item_netbox_url }}"
        netbox_token: "{{ item_netbox_token }}"
        validate_certs: False
        state: present
        data:
          device: "{{ item.device_name }}"
          name: "{{ item.interface_name }}"
          type: "{{ item.interface_type }}"
          mode: "{{ item.interface_mode }}"
          speed: "{{ item.interface_speed | int if item.interface_speed is number else omit }}"
          duplex: "{{ item.interface_duplex }}"
          enabled: "{{ item.interface_enabled }}"
          description: "{{ item.interface_description }}"
          untagged_vlan: "{{ {'vid': item.untagged_vlan} if item.untagged_vlan is defined and item.untagged_vlan is not none else omit }}"
          tagged_vlans: >-
            {%- if item.tagged_vlans is defined and item.tagged_vlans | length > 0 -%}
              {%- set vlans = [] -%}
              {%- for vlan in item.tagged_vlans -%}
                {%- set _ = vlans.append({
                    "name": "vlan-" ~ vlan,
                  }) -%}
              {%- endfor -%}
              {{ vlans }}
            {%- else -%}
              {{ omit }}
            {%- endif -%}
          custom_fields:
            PortProfile: "{{ item.custom_fields.PortProfile }}"
            voice_vlan: "{{ item.custom_fields.voice_vlan | int if item.custom_fields.voice_vlan is defined and item.custom_fields.voice_vlan is not none else omit }}"
            mac_sticky_address: "{{ item.custom_fields.mac_sticky_address | default('') }}"
            #port_security_aging: "{{ item.custom_fields.port_security_aging | default(false) }}"
            port_channel_mode: "{{ item.custom_fields.port_channel_mode | default('') }}"
            #speed_Mbps: "{{ item.custom_fields.speed_Mbps }}"
            portsecmax: "{{ item.custom_fields.portsecmax | int if item.custom_fields.portsecmax is defined and item.custom_fields.portsecmax is not none else omit }}"
            #portfast_status: "{{ item.custom_fields.portfast_status if item.custom_fields.portfast_status is defined and item.custom_fields.portfast_status is not none else omit }}"
            allowed_vlans_trunk: >-
              {%- if item.custom_fields.allowed_vlans_trunk is defined and item.custom_fields.allowed_vlans_trunk | length > 0 -%}
                {{ item.custom_fields.allowed_vlans_trunk }}
              {%- else -%}
                {{ omit }}
              {%- endif -%}
      loop: "{{ stack_device_interfaces }}"
      loop_control:
        label: "Creating interface {{ item.interface_name }} on device {{ item.device_name }}"
      #async: 9000  # Wait up to 5 minutes for each task to finish
      #poll: 0     # Do not block, let it run in the background
      
    - name: Stack - Block to create Port-channel
      block:

        - name: Stack - Create Port-channel if the mode and the number is attached
          netbox.netbox.netbox_device_interface:
            netbox_url: "{{ item_netbox_url }}"
            netbox_token: "{{ item_netbox_token }}"
            validate_certs: False
            state: present
            update_vc_child: true
            data:
              device: "{{ stack_masters | map(attribute='master_device') | first }}"
              name: "Port-channel{{ item.custom_fields.port_channel_number }}"
              type: "lag"
              mode: "{{ item.interface_mode }}"
              speed: "{{ item.interface_speed | int if item.interface_speed is number else omit }}"
              duplex: "{{ item.interface_duplex }}"
              enabled: "{{ item.interface_enabled }}"
              description: "{{ item.interface_description }}"
              untagged_vlan: "{{ {'vid': item.untagged_vlan} if item.untagged_vlan is defined and item.untagged_vlan is not none else omit }}"
              tagged_vlans: >-
                {%- if item.tagged_vlans is defined and item.tagged_vlans | length > 0 -%}
                  {%- set vlans = [] -%}
                  {%- for vlan in item.tagged_vlans -%}
                    {%- set _ = vlans.append({
                        "name": "vlan-" ~ vlan,
                      }) -%}
                  {%- endfor -%}
                  {{ vlans }}
                {%- else -%}
                  {{ omit }}
                {%- endif -%}
              custom_fields:
                PortProfile: "{{ item.custom_fields.PortProfile | default('') }}"
                mac_sticky_address: "{{ item.custom_fields.mac_sticky_address | default('') }}"
                #port_security_aging: "{{ item.custom_fields.port_security_aging | default(false) }}"
                port_channel_mode: "{{ item.custom_fields.port_channel_mode | default('') }}"
                #speed_Mbps: "{{ item.custom_fields.speed_Mbps | default('auto')  }}"
                allowed_vlans_trunk: >-
                  {%- if item.custom_fields.allowed_vlans_trunk is defined and item.custom_fields.allowed_vlans_trunk | length > 0 -%}
                    {{ item.custom_fields.allowed_vlans_trunk }}
                  {%- else -%}
                    {{ omit }}
                  {%- endif -%}
          loop: "{{ port_channel_interfaces }}"
          loop_control:
             label: >-
              {%- if item.custom_fields.port_channel_number is defined and item.custom_fields.port_channel_number is not none -%}
                Creating interface Port-channel{{ item.custom_fields.port_channel_number }} with physical interface {{ item.interface_name }} on device {{ item.device_name }}
              {%- else -%}
                Skipping Port-channel creation for interface {{ item.interface_name }} on device {{ item.device_name }}
              {%- endif -%}
          
        - name: Stack - Attach physical interface to the port-channel
          netbox.netbox.netbox_device_interface:
            netbox_url: "{{ item_netbox_url }}"
            netbox_token: "{{ item_netbox_token }}"
            validate_certs: False
            state: present
            update_vc_child: true
            data:
              device: "{{ stack_masters | map(attribute='master_device') | first }}"
              name: "{{ item.interface_name }}"
              lag: 
                name: "Port-channel{{ item.custom_fields.port_channel_number }}"
          loop: "{{ port_channel_interfaces }}"
          loop_control:
             label: >-
              {%- if item.custom_fields.port_channel_number is defined and item.custom_fields.port_channel_number is not none -%}
                Attaching interface Port-channel{{ item.custom_fields.port_channel_number }} to physical interface {{ item.interface_name }} on device {{ item.device_name }}
              {%- else -%}
                Skipping Port-channel creation for interface {{ item.interface_name }} on device {{ item.device_name }}
              {%- endif -%}

      when:
        - item.custom_fields.port_channel_number is defined and item.custom_fields.port_channel_number is not none
        - item.custom_fields.port_channel_mode is defined and item.custom_fields.port_channel_mode is not none
        - port_channel_interfaces is defined and port_channel_interfaces | length > 0
