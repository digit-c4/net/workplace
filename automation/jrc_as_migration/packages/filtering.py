import re 

def extract_fan_tray(parse):
    '''
    Extracts fan tray information (model and serial number) from a parsed configuration object.

    Parameters:
    parse (object): An object with an `ioscfg` attribute, which is a list of strings.
                    Each string represents a line in the device configuration.

    Returns:
    list: A list of dictionaries, where each dictionary contains:
        - 'fan_model': The product ID (PID) of the fan tray.
        - 'fan_serial_number': The serial number (SN) of the fan tray.

    If no fan tray details are found, an empty list is returned.
    '''
    
    # Join the lines from parse.ioscfg into a single string, `config_str`
    config_str = '\n'.join(parse.ioscfg)

    # Define a regular expression to match fan tray details:
    # - Looks for lines containing "Fan Tray" in the NAME field
    # - Captures the `PID` (product ID or model) and `SN` (serial number)
    fan_regex = re.compile(r"NAME:.*?Fan\s+Tray.*\nPID:\s+(.*?)\s.*?SN:\s+(\S+)")
    
    # Use `findall` to get all matches, each match will be a tuple (PID, SN)
    fan_match = fan_regex.findall(config_str)
    
    # Initialize an empty list to store each fan tray's details as dictionaries
    fan = []
    
    # Check if any matches were found
    if fan_match:
        # Iterate over each tuple (model, serial) in `fan_match`
        for item in fan_match:
            # Append a dictionary with `fan_model` and `fan_serial_number` to the list
            fan.append({
                "fan_model": item[0],
                "fan_serial_number": item[1]
            })
        
        # Return the list of fan tray details
        return fan
    else:
        # If no matches were found, return an empty list
        return []

def extract_power_supply_info(parse):
    """
    Extracts power supply information from a parsed configuration.

    This function searches the configuration for details on power supplies, capturing the switch name (if available),
    power supply identifier, and serial number. It returns these details as a list of dictionaries.

    Parameters:
    parse (object): Parsed configuration object with an `ioscfg` attribute.

    Returns:
    list: A list of dictionaries, each containing:
          - 'switch': The switch name or "Single Module" if not specified.
          - 'power_supply_id': The identifier for the power supply.
          - 'serial_number': The serial number of the power supply.
    """
    # Regular expression to capture switch name (if available), power supply identifier, and serial number
    #pattern = r'NAME: "(?:(Switch\s+\d+)\s+-\s+)?Power\s+Supply\s+(?:Module\s+)?([A-Z0-9])".*?\nPID: .*, VID: .*, SN:\s+(\w+)'
    pattern = r'NAME: "(?:(Switch\s+\d+)\s+-\s+)?Power\s+Supply\s+(?:Module\s+)?([A-Z0-9])".*?\nPID:\s+(\S+).*, SN:\s+(\w+)'
    config_str = '\n'.join(parse.ioscfg)
    
    # Find all matches in the text
    matches = re.findall(pattern, config_str)
    
    # Format the results into a list of dictionaries
    results = []
    for match in matches:
        # Extract the switch name, power supply identifier, and serial number
        switch_name = match[0] if match[0] else "Single Module"
        power_supply_id = match[1]
        model = match[2]
        serial_number = match[3]
        
        # Append the data as a dictionary to results
        results.append({
            "switch": switch_name,
            "power_supply_id": power_supply_id,
            "model": model,
            "serial_number": serial_number
        })
    
    return results

def extract_system_serial_numbers(parse):
    """
    Extract the 'System Serial Number' from the Cisco device parse for each switch.
    Includes show version and show module handling in case of stack.
    
    :param parse: The text parse from the Cisco device
    :return: A list of dictionaries with switch number, system serial number, and version
    """
    config_str = '\n'.join(parse.ioscfg)

    # Extract hostname
    hostname_regex = re.compile(r"(\S+)\s+uptime\s+is\s+\d+\s+\S+")
    hostname_match = hostname_regex.search(config_str)
    hostname = hostname_match.group(1) if hostname_match else "switch"

    # Extract version
    version_regex = re.compile(r",\s+Version\s+(.*?),")
    version_match = version_regex.search(config_str)
    version = version_match.group(1) if version_match else "Unknown"

    # Extract switch numbers, serial numbers from "show module"
    module_regex = re.compile(r"^\s*(\d{1,2})\s+\d+\s+\S+\s+([A-Z0-9]{8,12})\s+\S+\s+\S+", re.MULTILINE)
    module_matches = module_regex.findall(config_str)

    # Determine if the device is a stack based on module entries
    is_stack = len(module_matches) > 1

    # Combine switch numbers, serial numbers, and version with stack logic
    serial_numbers = []
    if is_stack:
        for match in module_matches:
            switch_number, switch_sn = match
            serial_entry = {
                "hostname": f"{hostname}-{switch_number}",
                "system_serial_number": switch_sn,
                "version": version,
                "stack_name": hostname,
                "stack_role": switch_number
            }
            serial_numbers.append(serial_entry)
        
    elif len(module_matches) == 1:
        # Single device case if only one module entry is found
        switch_number, switch_sn = module_matches[0]
        serial_numbers.append({
            "hostname": hostname,
            "system_serial_number": switch_sn,
            "version": version
        })
        
    else:
        # Use 'show version' serial if no module entries are found
        serial_regex = re.compile(r"System .erial .umber\s*:\s*(\S+)")
        serial_match = serial_regex.search(config_str)
        if serial_match:
            serial_numbers.append({
                "hostname": hostname,
                "system_serial_number": serial_match.group(1),
                "version": version
            })
        else:
            raise Exception('Could not find the SN/version')

    return serial_numbers

def get_ap_interfaces(parse):
    """
    Extracts AP (Access Point) interfaces from CDP neighbor details in the configuration.

    This function searches for Cisco AIR devices in the configuration by looking for specific
    patterns related to AP interfaces. If matches are found, it returns a list of AP interfaces.

    Parameters:
    parse (object): Parsed configuration object with an `ioscfg` attribute.

    Returns:
    list: A list of AP interface names extracted from CDP neighbor details, or an empty list if none are found.
    """
    ap_interfaces = []
    config_str = '\n'.join(parse.ioscfg) # Join the configuration lines into a single string
    
    # Extract AP interfaces using regex for Cisco AIR devices from CDP neighbor details
    #regex_ap = re.compile(r"Platform:\s+cisco\s+(C9136[a-zA-Z]-[a-zA-Z]|AIR-.*?),\s+.*\nInterface:\s+(.*?),")
    regex_ap = re.compile(r"Capabilities:.*?Trans-Bridge.*\nInterface:\s+(.*?),")
    
    match_ap = regex_ap.findall(config_str)
    
    if match_ap:
        for interface in match_ap:
            #ap_interface = interface[1]
            #ap_interfaces.append(ap_interface)
            ap_interfaces.append(interface)
    else:
        return []
    
    return ap_interfaces

def get_channel_group_mode(parse):
    '''Extracts channel group mode and port-channel number for each interface in a parsed configuration.

    This function identifies interfaces with a defined channel group mode (e.g., "active," "passive").
    For each matching interface, it records the mode and port-channel number in a dictionary.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a tuple containing:
          - Mode of the channel group (e.g., 'active', 'passive')
          - Port-channel number as a string.
    '''
    mapping_channel_group_mode = {}
    
    # Search for interfaces that have a channel-group with a mode defined
    for intf_obj in parse.find_objects_w_child('^interface', r'^\s+channel-group\s+\d+\smode\s+(\w+)'):
        name_of_interface = intf_obj.text.replace('interface ', '').strip()

        # Find the child line with channel-group and mode
        channel_group_line = intf_obj.re_search_children(r'^\s+channel-group\s+\d+\smode\s+(\w+)')
        if channel_group_line:
            match = re.search(r'channel-group\s+(\d+)\smode\s+(\w+)', channel_group_line[0].text)
            if match:
                po_number = match.group(1)  # Port-channel number
                mode = match.group(2)       # Mode (e.g., active, passive, on)
                
                # Map the interface name to a tuple of (mode, port-channel number)
                mapping_channel_group_mode[name_of_interface] = (mode, po_number)
    
    return mapping_channel_group_mode

def get_channel_group_mode(parse):
    '''Extracts channel group mode and port-channel number for each interface in a parsed configuration.

    This function identifies interfaces with a defined channel group mode (e.g., "active," "passive").
    For each matching interface, it records the mode and port-channel number in a dictionary.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a tuple containing:
          - Mode of the channel group (e.g., 'active', 'passive')
          - Port-channel number as a string.
    '''
    mapping_channel_group_mode = {}
    
    # Search for interfaces that have a channel-group with a mode defined
    for intf_obj in parse.find_objects_w_child('^interface', r'^\s+channel-group\s+\d+\smode\s+(\w+)'):
        name_of_interface = intf_obj.text.replace('interface ', '').strip()

        # Find the child line with channel-group and mode
        channel_group_line = intf_obj.re_search_children(r'^\s+channel-group\s+\d+\smode\s+(\w+)')
        if channel_group_line:
            match = re.search(r'channel-group\s+(\d+)\smode\s+(\w+)', channel_group_line[0].text)
            if match:
                po_number = match.group(1)  # Port-channel number
                mode = match.group(2)       # Mode (e.g., active, passive, on)
                
                # Map the interface name to a tuple of (mode, port-channel number)
                mapping_channel_group_mode[name_of_interface] = (mode, po_number)
    
    return mapping_channel_group_mode

def get_channel_group_mode(parse):
    '''Extracts channel group mode and port-channel number for each interface in a parsed configuration.

    This function identifies interfaces with a defined channel group mode (e.g., "active," "passive").
    For each matching interface, it records the mode and port-channel number in a dictionary.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a tuple containing:
          - Mode of the channel group (e.g., 'active', 'passive')
          - Port-channel number as a string.
    '''
    mapping_channel_group_mode = {}
    
    # Search for interfaces that have a channel-group with a mode defined
    for intf_obj in parse.find_objects_w_child('^interface', r'^\s+channel-group\s+\d+\smode\s+(\w+)'):
        name_of_interface = intf_obj.text.replace('interface ', '').strip()

        # Find the child line with channel-group and mode
        channel_group_line = intf_obj.re_search_children(r'^\s+channel-group\s+\d+\smode\s+(\w+)')
        if channel_group_line:
            match = re.search(r'channel-group\s+(\d+)\smode\s+(\w+)', channel_group_line[0].text)
            if match:
                po_number = match.group(1)  # Port-channel number
                mode = match.group(2)       # Mode (e.g., active, passive, on)
                
                # Map the interface name to a tuple of (mode, port-channel number)
                mapping_channel_group_mode[name_of_interface] = (mode, po_number)
    
    return mapping_channel_group_mode

def get_dot1x_interface(parse):
    '''
    Try get dot1x from the interface. Return True if 'dot1x' field is found.
    '''
    dot1x = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        dot1x[name_of_interface] = False
        for child_line in intf_obj.re_search_children(r'dot1x|mab'):
            dot1x[name_of_interface] = True
            break
    return dot1x

def get_duplex(parse):
    '''Extracts duplex configuration for each interface from a parsed configuration object.

    This function iterates over each interface configuration to retrieve the interface name and 
    its duplex setting (if specified). If no specific duplex setting is configured, it defaults to 'auto'.
    The information is stored in a dictionary where the interface name is the key, 
    and the duplex setting is the value.

    Parameters:
    parse (object): Parsed configuration object that supports methods to locate interface configurations 
                    and match specific configuration lines.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is the duplex setting 
          as a string (e.g., 'full', 'half', or 'auto' if not explicitly set).
    '''
    duplex_mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        duplex = intf_obj.re_match_iter_typed(r'duplex\s(\w+)', result_type=str, group=1, default='auto')
        duplex_mapping[name_of_interface] = duplex
    return duplex_mapping

def get_enabled_status(parse):
    '''Determines the enabled status of each interface from a parsed configuration.

    Checks each interface for a 'shutdown' command to determine if it is disabled. 
    Stores the interface name and its status (False if shutdown is configured).

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is False 
          if 'shutdown' is configured, indicating the interface is disabled.
    '''
    enabled_mapping = {}
    for intf_obj in parse.find_objects_w_child('^interface', '^\s+shutdown'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        enabled_mapping[name_of_interface] = False
    return enabled_mapping

def get_excluded_interfaces_downlinks(parse):
    """
    Extract interfaces associated with distribution switch from the configuration file from CDP neighbor detail.
    Those interfaces will be marked with profile downlink.
    Also the port-channels associated with those interfaces will be marked with profile downlink.
    """
    excluded_interfaces_downlinks = []
    #Get the physical interfaces of a switch connected to another switch e.g. 9300-3650
    config_str = '\n'.join(parse.ioscfg)
    match_downlinks_geel = re.findall(r'Platform:\s+cisco\s+(C9300-.*?|WS-C2960.*?|WS-C3560.*?),.*?\n.*?Interface:\s+(.*?),',config_str, re.DOTALL)
    
    for match_downlink in match_downlinks_geel:
        excluded_interfaces_downlinks.append(match_downlink[1].strip())
    
    #List to temporarily store the port-channels
    #port_channels_to_exclude1 = []
    #for interface in excluded_interfaces_downlinks:
    #    port_channel_match1 = re.findall(r'interface '+ interface + r'.*?channel-group (\d+) mode', config_str, re.DOTALL)
    #    if port_channel_match1:
    #        port_channel_num = port_channel_match1[0]
    #        port_channels_to_exclude1.append(f'Port-channel{port_channel_num}')
    #        print(f'the interface {interface} has port-channel {port_channel_num}')

    #excluded_interfaces_downlinks = list(set(excluded_interfaces_downlinks + port_channels_to_exclude1))
    return excluded_interfaces_downlinks

def get_excluded_interfaces_uplinks(parse):
    """
    Extract interfaces associated with distribution switch from the configuration file from CDP neighbor detail.
    e.g. name-tc-dsxyz
    Those interfaces will be marked with profile uplink.
    Also the port-channels associated with those interfaces will be marked with profile uplink.
    """
    excluded_interfaces = []
    #Get the physica interfaces connected to the DS
    config_str = '\n'.join(parse.ioscfg)
    matches = re.findall(r"Device ID: .*?-tc-ds0[1-9]\n.*?Interface: (.*?),", config_str, re.DOTALL)
    for match in matches:
        excluded_interfaces.append(match.strip())
    
    ##Get the physical interfaces of a switch connected to another switch e.g. 3650-9300 (Geel use case)
    #match_uplinks_3560 = re.findall(r'Platform: cisco C9300.*?\n.*?Interface: (.*?),',config_str, re.DOTALL)
    #for match_uplink in match_uplinks_3560:
    #    excluded_interfaces.append(match_uplink.strip())
        
    # List to temporarily store the uplink port-channels
    port_channels_to_exclude = []
    for interface in excluded_interfaces:
        port_channel_match = re.findall(r'interface '+ interface + r'.*?channel-group (\d+) mode', config_str, re.DOTALL)
        if port_channel_match:
            port_channel_num = port_channel_match[0]
            port_channels_to_exclude.append(f'Port-channel{port_channel_num}')
    excluded_interfaces = list(set(excluded_interfaces + port_channels_to_exclude))
    return excluded_interfaces

def get_interface_type(parse):
    """
    Extract the type of interface (e.g., 100gbase, 40gbase) from the configuration.
    """
    mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        interface_name = str(intf_obj.text).replace('interface ', '')
        # Check the interface type based on its name and set its corresponding type
        if "Hundred" in interface_name:
            type_of_interface = "100gbase-x-qsfp28"
        elif "Forty" in interface_name:
            type_of_interface = "40gbase-x-qsfpp"
        elif "Twe" in interface_name:
            type_of_interface = "25gbase-x-sfp28"
        elif "Ten" in interface_name:
            type_of_interface = "10gbase-x-sfpp"
        elif "Giga" in interface_name:
            type_of_interface = "1000base-t"
        elif "Fast" in interface_name:
            type_of_interface = "100base-tx"
        elif "Port" in interface_name:
            type_of_interface = "lag"
        else:
            type_of_interface = "virtual"
        mapping[interface_name] = type_of_interface
    return mapping

def get_ip_interface(parse):
    '''Extracts IP address and subnet mask information for each interface from a parsed configuration object.

    This function iterates over each interface configuration, retrieving the interface name, IP address, 
    and subnet mask (if available). It stores these details in a dictionary where the interface name is 
    the key, and the IP address and subnet mask are stored in a nested dictionary.

    Parameters:
    parse (object): Parsed configuration object with methods to find and match specific configuration lines.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is another dictionary containing:
        - 'ip': The IP address assigned to the interface, or an empty string if none is found.
        - 'mask': The subnet mask associated with the IP address, or an empty string if none is found.
    '''
    ip_mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        ipaddress = intf_obj.re_match_iter_typed(r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)\s', result_type=str, group=1, default='')
        netmask = intf_obj.re_match_iter_typed(r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)\s(\d+\.\d+\.\d+\.\d+)', result_type=str, group=2, default='')
        ip_mapping[name_of_interface] = {'ip': ipaddress, 'mask': netmask}
    return ip_mapping

def get_line_cards_supervisors(parse):
    """
    Extract the line cards and supervisors for the modular switches e.g. 9410.
    """
    config_str = '\n'.join(parse.ioscfg)
    line_card_regex = re.compile(r'NAME:\s+"Slot\s+(\d+)\s+Linecard".*\nPID:\s+(\S+).+SN:\s+(\S+)')
    line_card_match = line_card_regex.findall(config_str)
    line_cards = []
    if line_card_match:
        for item in line_card_match:
            line_card_slot = item[0]
            line_card_model = item[1]
            line_card_sn = item[2]
            line_cards.append({
                'line_card_slot': line_card_slot,
                'line_card_model': line_card_model,
                'line_card_sn': line_card_sn
            })

    supervisor_regex = re.compile(r'NAME:\s+"Slot\s+(\d+)\s+Supervisor".*\nPID:\s+(\S+).+SN:\s+(\S+)')
    supervisor_match = supervisor_regex.findall(config_str)
    supervisor = []
    if supervisor_match:
        for item in supervisor_match:
            supervisor_slot = item[0]
            supervisor_model = item[1]
            supervisor_sn = item[2]
            supervisor.append({
                "supervisor_slot": supervisor_slot,
                "supervisor_model": supervisor_model,
                "supervisor_sn": supervisor_sn
            })

    if line_cards:
        return line_cards + supervisor
    else:
        return []

def get_mac_address(parse):
    '''Extracts manually configured MAC addresses for each interface from a parsed configuration.

    For each interface, this function collects MAC addresses set with "switchport port-security mac-address."
    If any are found, they are joined into a comma-separated string; otherwise, an empty string is recorded.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a comma-separated string of 
          configured MAC addresses, or an empty string if none are set.
    '''
    mapping_address = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        mac_addresses = []
        for child in intf_obj.children:
            match = re.search(r'switchport\sport-security\smac-address\s(\S+)', child.text)
            if match:
                mac_addresses.append(match.group(1))
        mapping_address[name_of_interface] = ', '.join(mac_addresses) if mac_addresses else ""
    return mapping_address

def get_mac_sticky_maximum(parse):
    '''Extracts the maximum number of allowed sticky MAC addresses for each interface from a parsed configuration.

    For each interface, this function finds the maximum MAC address limit set with 
    "switchport port-security maximum." If found, it stores the limit as an integer; otherwise, 
    the interface is omitted from the result.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is the maximum 
          number of sticky MAC addresses allowed as an integer.
    '''
    mapping_sticky_maximum = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        maximum_mac_addresses = []
        for child in intf_obj.children:
            match = re.search(r'\s*switchport port-security maximum (\d+)', child.text)
            if match:
                maximum_mac_addresses.append(match.group(1))
        for element in maximum_mac_addresses:
            element = eval(element)
            mapping_sticky_maximum[name_of_interface] = element
    return mapping_sticky_maximum

def get_mac_sticky(config):
    '''Extracts sticky MAC addresses for each interface from a parsed configuration.

    For each interface, this function collects MAC addresses configured with "port-security mac-address sticky."
    If found, it joins them into a comma-separated string; otherwise, it records an empty string.

    Parameters:
    config (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a string of comma-separated 
          sticky MAC addresses, or an empty string if none are configured.
    '''
    mapping_sticky = {}
    for intf_obj in config.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        mac_addresses = []
        for child in intf_obj.children:
            match = re.search(r'port-security\smac-address\ssticky\s(\S+)', child.text)
            if match:
                mac_addresses.append(match.group(1))
        mapping_sticky[name_of_interface] = ','.join(mac_addresses)
    return mapping_sticky

def get_pid(parse):
    '''Extracts the PID (Product ID) from a parsed configuration.

    This function searches for the "PID" field in the configuration. If found, it returns 
    the PID; if not, it raises an exception.

    Parameters:
    parse (object): Parsed configuration object with an `ioscfg` attribute.

    Returns:
    str: The PID found in the configuration.

    Raises:
    Exception: If no PID is found in the configuration.
    '''
    config_str = '\n'.join(parse.ioscfg)
    pid_regex = re.compile(r"PID:\s+(\S+)")
    pid_match = pid_regex.search(config_str)

    if pid_match:
        pid = pid_match.group(1)
        return pid
    else:
        raise Exception('No PID found in show inventory')
    
def get_port_security_aging(parse):
    '''Checks if port security aging is configured for each interface in a parsed configuration.

    For each interface, this function looks for the "switchport port-security aging" command. 
    If found, it marks the interface as having port security aging enabled.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a boolean:
          - True if port security aging is configured.
          - False if it is not configured.
    '''
    port_security_aging_mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        port_security_aging_mapping[name_of_interface] = False
        for child_line in intf_obj.re_search_children(r'^\s*switchport\s+port-security\s+aging\s(\S+)'):
            port_security_aging_mapping[name_of_interface] = True
    return port_security_aging_mapping

def get_port_security(parse):
    '''Checks port security settings for each interface in a parsed configuration.

    For each interface, this function searches for the presence of the "switchport port-security" command.
    If found, it records the command; otherwise, it stores None for that interface.

    Parameters:
    parse (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is the port security command 
          if configured, or None if not.
    '''
    mapping_port_security = {}
    for intf_obj in parse.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        port_security_command = None
        for child in intf_obj.children:
            if re.match(r'^\s*switchport port-security\s*$', child.text):
                port_security_command = child.text.strip()
                break
        mapping_port_security[name_of_interface] = port_security_command
    return mapping_port_security

def get_speed(parse):
    '''Extracts the speed configuration for each interface from a parsed configuration object.

    This function iterates over each interface configuration, retrieving the interface name and 
    its configured speed (if specified). If no explicit speed is set, it defaults to 'auto'.
    The details are stored in a dictionary where the interface name is the key, 
    and the speed setting is the value.

    Parameters:
    parse (object): Parsed configuration object that supports finding interface configurations 
                    and matching specific configuration lines.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is the configured 
          speed as a string, or 'auto' if no specific speed setting is found.
    '''
    speed_mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        interface_name = str(intf_obj.text).replace('interface ', '').strip()
        speed = intf_obj.re_match_iter_typed(r'speed\s(\d+)', result_type=str, group=1, default="")
        if speed:
            speed_mapping[interface_name] = speed
        else:
            speed_mapping[interface_name] = 'auto'
    return speed_mapping

def get_stack(config):
    """
    Extract switch numbers, roles, MAC addresses, priority, version, and state from the configuration.

    Args:
        config (str): Configuration string of the switch stack.

    Returns:
        dict: A dictionary where keys are switch numbers and values are dictionaries
              with details such as "role", "mac_address", "priority", "version", and "state".
    """
    stack_info = {}
    stack_regex = re.compile(
        r"(\*?)\s*(\d+)\s+(\w+)\s+([\da-f\.]+)\s+(\d+)\s+(V\d+)\s+(\w+)"
    )

    # Find all matches in the provided config
    stack_match = stack_regex.findall(config)

    if stack_match:
        for match in stack_match:
            is_active = True if match[0] == "*" else False  # Active switch is marked with "*"
            switch_number = match[1]
            stack_info[switch_number] = {
                "active": is_active,
                "role": match[2],
                "mac_address": match[3],
                "priority": match[4],
                "version": match[5],
                "state": match[6],
            }

    return stack_info

def get_sticky(config):
    '''Extracts sticky for each interface from a parsed configuration.

    For each interface, this function collects sticky configured with "port-security mac-address sticky."
    If found, it returns true; otherwise, it records an empty False.

    Parameters:
    config (object): Parsed configuration object.

    Returns:
    dict: A dictionary where each key is an interface name, and each value is a boolean with TRUE if it`s configured and FALSE if it`s not configured.
    '''
    mapping_sticky = {}
    for intf_obj in config.find_objects('^interface'):
        name_of_interface = str(intf_obj.text).replace('interface ', '').strip()
        for child in intf_obj.children:
            match = re.search(r'port-security\s+mac-address\s+sticky$', child.text)
            if match:
                mapping_sticky[name_of_interface] = True
                break
    return mapping_sticky

def get_supervisor_4500(parse):
    """
    Extract supervisor module details for Cisco 4500 series from the configuration.
    """
    # Search for lines related to the line card or supervisor description
    for line in parse.find_lines(r'NAME: ".*?slot .*?", DESCR: ".*Supervisor'):
        # Detect Supervisor II+
        sup_2_match = re.search(r'DESCR: ".*(Supervisor II\+)', line)
        if sup_2_match:
            return [sup_2_match.group(1)]
        
        # Detect Sup 7-[A-Z]
        sup_7_match = re.search(r'DESCR: ".*(Sup 7.*-[A-Z])', line)
        if sup_7_match:
            return [sup_7_match.group(1)]
        
        # Detect Sup 6-[A-Z]
        sup_6_match = re.search(r'DESCR: ".*(Supervisor 6.*-[A-Z])', line)
        if sup_6_match:
            return [sup_6_match.group(1)]
        
        # Detect Sup 8-[A-Z]
        sup_8_match = re.search(r'DESCR: ".*(Sup 8.*-[A-Z])', line)
        if sup_8_match:
            return [sup_8_match.group(1)]
    
    # If no supervisors are found, return an empty list
    return []

def get_uplink_module(parse):
    config_str = '\n'.join(parse.ioscfg)

    uplink_regex= re.compile(r"Switch\s+(\d+).*?FRU.*\nPID:\s+(\S+).*?SN:\s+(\S+)")
    uplink_match = uplink_regex.findall(config_str)

    result = []
    if uplink_match:
        for uplink in uplink_match:
            result.append({
                "switch_id": uplink[0],
                "uplink_model": uplink[1],
                "uplink_serial_number": uplink[2]
            })
        return result
    else:
        return []

def get_vlan_and_mode(parse):
    """
    Extract VLAN mode (access or trunk), VLANs, and native VLANs from the configuration.
    """
    mode_mapping = {}
    vlan_mapping = {}
    native_vlan_mapping = {}
    voice_vlan_mapping = {}

    # Loop through each interface in the configuration
    for intf_obj in parse.find_objects('^interface'):
        interface_name = str(intf_obj.text).replace('interface ', '').strip()

        # Initialize mode as None
        mode = None
        voice_vlan = None

        # Determine the switchport mode (access or trunk)
        for child in intf_obj.children:
            match_mode = re.search(r'switchport mode (\w+)', child.text)
            if match_mode:
                mode = match_mode.group(1)
                break
                
        # Determine the switchport mode when only have 'switchport access' on the interface
        for child in intf_obj.children:
            access_match_mode = re.search(r'switchport access', child.text)
            if access_match_mode:
                mode = 'access'
                break
        
        # Determine the switchport mode for voice
        for child in intf_obj.children:
            voice_match_mode = re.search(r'switchport voice', child.text)
            if voice_match_mode:
                mode = 'access'
                break

        # Process access mode
        if mode == "access":
            vlanaccess = intf_obj.re_match_iter_typed(r'switchport\saccess\svlan\s(\d+)', 
                                                      result_type=str, group=1, default=None)
            if vlanaccess and vlanaccess.isdigit():
                mode_mapping[interface_name] = "access"
                vlan_mapping[interface_name] = int(vlanaccess)  # Safely convert to int

            # Process the voice VLAN if present
            voice_vlan = intf_obj.re_match_iter_typed(r'switchport\s+voice\s+vlan\s+(\d+)', 
                                                      result_type=str, group=1, default=None)
            if voice_vlan and voice_vlan.isdigit():
                mode_mapping[interface_name] = "access"
                voice_vlan_mapping[interface_name] = int(voice_vlan)

        # Process trunk mode
        elif mode == "trunk":
            vlantrunk = intf_obj.re_match_iter_typed(r'switchport\strunk\sallowed\svlan\s([\d, -]+)', 
                                                     result_type=str, group=1, default='')

            vlantrunk_list = []
            if vlantrunk:  # Only process if vlantrunk is not an empty string
                for vlan in vlantrunk.split(','):
                    if '-' in vlan:  # Handle ranges like "100-200"
                        try:
                            start, end = map(int, vlan.split('-'))
                            vlantrunk_list.extend(range(start, end + 1))
                        except ValueError:
                            pass  # Ignore invalid ranges
                    else:
                        if vlan.isdigit():  # Check if it's a valid number
                            vlantrunk_list.append(int(vlan))

            # Save mode and allowed VLANs
            mode_mapping[interface_name] = "trunk"
            vlan_mapping[interface_name] = vlantrunk_list

            native_vlan = intf_obj.re_match_iter_typed(r'switchport trunk native vlan (\d+)', result_type=str, group=1, default='')

            # Only convert to an integer if native_vlan is not empty
            if native_vlan and native_vlan.isdigit():
                native_vlan_mapping[interface_name] = int(native_vlan)


    return mode_mapping, vlan_mapping, native_vlan_mapping, voice_vlan_mapping

def parse_descriptions(parse):
    '''Extracts interface descriptions from a parsed configuration object.

    This function iterates over interface configurations, retrieves each interface's name, 
    and its corresponding description (if any). It then stores these as key-value pairs 
    in a dictionary where the interface name is the key, and the description is the value.

    Parameters:
    parse (object): Parsed configuration object with methods to find and extract configuration lines.

    Returns:
    dict: A dictionary mapping each interface name to its description, or an empty string if no description is present.
    '''
    
    description_mapping = {}
    for intf_obj in parse.find_objects('^interface'):
        interface_name = str(intf_obj.text).replace('interface ', '')
        description = intf_obj.re_match_iter_typed(r'description\s+(\S.*)', result_type=str, group=1, default='')
        description_mapping[interface_name] = description
    return description_mapping