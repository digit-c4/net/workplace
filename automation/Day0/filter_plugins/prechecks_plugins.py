from ciscoconfparse import CiscoConfParse

def switch_interface_number_old(config_text):
    ''' Extract all old switch interface names from the show running configuration with the parameters. '''
    config_lines = config_text.split('\n')
    parser = CiscoConfParse(config_lines)
    interfaces = {}

    # Parse all interfaces
    for intf_obj in parser.find_objects(r"^interface\s+(Fa|Eth|Gi|Te|Twe|Hun|TwentyFiveGigE|HundredGigE)\S+"):

        # Remove the 'interface' from the matched object
        intf_name = intf_obj.text.replace('interface ', '')

        if intf_name not in interfaces:
            interfaces[intf_name] = []
        for c_obj in intf_obj.children:
            interfaces[intf_name].append(c_obj.text)

    return interfaces

class FilterModule(object):
    ''' Custom filters for extracting and replacing interface names in Cisco configurations'''

    def filters(self):
        return {
            'switch_interface_number_old': switch_interface_number_old
        }
