#time ansible-playbook playbook-import-ds-2-netbox.yml -e 'target=c9600 ip=7.8.9.41/24 site=ispra'

---

- name: Import DS in NetBox
  hosts: "{{ target }}"
  gather_facts: False
  vars:
    item_netbox_url: "{{ lookup('env', 'NETBOX_URL') }}"
    item_netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"

  tasks:

    - name: Fail if not all the parameters are provided
      fail:
        msg: "Not all the parameters were provided. Stopping..."
      when:
        - (hostname is not defined or hostname | length == 0)
        - (ip is not defined or ip | length == 0)
        - (mgmt_vlan is not defined or mgmt_vlan | length == 0)
        - (site is not defined or site | length == 0)

    - name: Install python package
      ansible.builtin.pip:
        name: 
          - pynetbox==7.3.3
          - genie==24.7
          - pyats==24.7
          - ntc-templates==5.1.0
    
    - name: Initialize empty dict to transform the data for NetBox
      set_fact:
        netbox_data: {}
    
    - name: Run command show version
      ansible.utils.cli_parse:
        command: show version
        parser:
          name: ansible.netcommon.pyats
        set_fact: version_result
      ignore_errors: yes
    
    - name: Extract the hostname
      set_fact:
        device_name: "{{ version_result['version']['hostname']  }}"
    
    - name: Extract details from show version
      set_fact:
        switches: >-
          {%- set switches = [] -%}
          {%- if version_result is defined and version_result['version'] is defined -%}
            {%- for switch_id, switch_data in version_result['version']['switch_num'].items() -%}
              {%- set switch_entry = {
                'device_name': version_result['version']['hostname'] ~ ('-' ~ switch_id if version_result['version']['switch_num'] | length > 1 else ''),
                'system_sn': switch_data['system_sn'] if 'system_sn' in switch_data else None,
                'stack_name': version_result['version']['hostname'] if version_result['version']['switch_num'] | length > 1 else None,
                'vc_priority': 16 - switch_id | int if version_result['version']['switch_num'] | length > 1 else None,
                'is_stack': false if version_result['version']['switch_num'] | length == 1 else true,
                'model': switch_data['model_num'] if 'model_num' in switch_data else None,
                'version': version_result['version']['version'],
                'is_stack_master': false
              } -%}
              {%- set _ = switches.append(switch_entry) -%}
            {%- endfor -%}
          {%- endif -%}
          {{ switches }}
    
    - name: Run command inventory
      ansible.utils.cli_parse:
        command: show inventory
        parser:
          name: ansible.netcommon.ntc_templates
        set_fact: inventory_result
      ignore_errors: yes
    
    - name: Extract the Power Supply
      set_fact:
        power_supply: >-
          {%- set power_supplies = [] -%}
          {%- for item in inventory_result -%}
            {%- set power_supply_info = {} -%}
            {%- if 'witch' in item['name'] and 'upply' in item['name'] -%}
              {%- set switch_number = item['name'].split()[1] -%}
              {%- set power_supply_info = {
                'device_name': device_name ~ '-' ~ switch_number,
                'power_supply_id': item['name'].split()[-1],
                'pid': item['pid'],
                'sn': item['sn']
              } -%}
            {%- elif 'upply' in item['name']  -%}
              {%- set power_supply_info = {
                'device_name': device_name,
                'power_supply_id': item['name'].split()[-1],
                'pid': item['pid'],
                'sn': item['sn']
              } -%}
            {%- endif -%}
            {%- if power_supply_info -%}
              {%- set _ = power_supplies.append(power_supply_info) -%}
            {%- endif -%}
          {%- endfor -%}
          {{ power_supplies }}
    
    - name: Extract the fan trays
      set_fact:
        fan_tray: >-
          {%- set fan_tray = [] -%}
          {%- for item in inventory_result -%}
            {%- set fan_tray_info = {} -%}
            {%- if 'witch' in item['name'] and 'Tray' in item['name'] -%}
              {%- set switch_number = item['name'].split()[1] -%}
              {%- set fan_tray_info = {
                'device_name': device_name ~ '-' ~ switch_number,
                'fan_supply_id': item['name'].split()[-1],
                'pid': item['pid'],
                'sn': item['sn']
              } -%}
            {%- elif 'Tray' in item['name']  -%}
              {%- set fan_tray_info = {
                'device_name': device_name,
                'fan_supply_id': item['name'].split()[-1],
                'pid': item['pid'],
                'sn': item['sn']
              } -%}
            {%- endif -%}
            {%- if fan_tray_info -%}
              {%- set _ = fan_tray.append(fan_tray_info) -%}
            {%- endif -%}
          {%- endfor -%}
          {{ fan_tray }}
    
    - name: Extract the supervisors
      set_fact:
        supervisor: >- 
          {%- set supervisors = [] -%}
            {%- for item in inventory_result -%}
              {%- set supervisors_info = {} -%}
              {%- if 'witch' in item['name'] and 'upervisor' in item['name'] -%}
                {%- set switch_number = item['name'].split()[1] -%}
                {%- set supervisors_info = {
                  'device_name': device_name ~ '-' ~ switch_number,
                  'supervisor_id': item['name'].split()[3],
                  'pid': item['pid'],
                  'sn': item['sn']
                } -%}
              {%- elif 'upervisor' in item['name']  -%}
                {%- set supervisors_info = {
                  'device_name': device_name,
                  'supervisor_id': item['name'].split()[1],
                  'pid': item['pid'],
                  'sn': item['sn']
                } -%}
              {%- endif -%}
              {%- if supervisors_info -%}
                {%- set _ = supervisors.append(supervisors_info) -%}
              {%- endif -%}
            {%- endfor -%}
            {{ supervisors }}
    
    - name: Extract the linecards
      set_fact:
        linecards: >- 
          {%- set linecards = [] -%}
            {%- for item in inventory_result -%}
              {%- set linecards_info = {} -%}
              {%- if 'witch' in item['name'] and 'inecard' in item['name'] -%}
                {%- set switch_number = item['name'].split()[1] -%}
                {%- set linecards_info = {
                  'device_name': device_name ~ '-' ~ switch_number,
                  'linecards_id': item['name'].split()[3],
                  'pid': item['pid'],
                  'sn': item['sn']
                } -%}
              {%- elif 'inecard' in item['name']  -%}
                {%- set linecards_info = {
                  'device_name': device_name,
                  'linecards_id': item['name'].split()[1],
                  'pid': item['pid'],
                  'sn': item['sn']
                } -%}
              {%- endif -%}
              {%- if linecards_info -%}
                {%- set _ = linecards.append(linecards_info) -%}
              {%- endif -%}
            {%- endfor -%}
            {{ linecards }}
    
    - name: Extract SFPs
      set_fact:
        sfps: >-
          {%- set sfps = [] -%}
          {%- for item in inventory_result -%}
            {%- if 'LX' in item['descr'] or 'GE T' in item['descr'] or 'SX' in item['descr'] or 'SFP' in item['descr'] -%}
              {%- set stack_devices = switches | selectattr('is_stack', 'equalto', true) | list -%}
              {%- set sfps_info = {} -%}
              {%- if stack_devices | length > 0 -%}
                {%- set interface_match = item['name'] | regex_search('(\d+).*?\/', '\\1') | first if item['name'] is defined else '0' -%}
                {%- set sfps_info = {
                  'device_name': device_name ~ '-' ~ interface_match,
                  'interface': item['name'] if item['name'] is defined else 'unknown',
                  'pid': item['pid'] if 'pid' in item else 'unknown',
                  'sn': item['sn'] if 'sn' in item else 'unknown'
                } -%}
              {%- else -%}
                {%- set sfps_info = {
                  'device_name': device_name,
                  'interface': item['name'] if item['name'] is defined else 'unknown',
                  'pid': item['pid'] if 'pid' in item else 'unknown',
                  'sn': item['sn'] if 'sn' in item else 'unknown'
                } -%}
              {%- endif -%}
              {%- set _ = sfps.append(sfps_info) -%}
            {%- endif -%}
          {%- endfor -%}
          {{ sfps }}

    - name: Determine stack master devices (only if stack is detected)
      set_fact:
        stack_masters: >-
          {%- if switches | map(attribute='is_stack') | first == true -%}
            {%- set masters = [] -%}
            {%- for stack_name in switches | map(attribute='stack_name') | unique -%}
              {%- set devices_in_stack = (switches | selectattr('stack_name', 'equalto', stack_name) | sort(attribute='vc_priority', reverse=True)) -%}
              {%- set master_device = devices_in_stack[0]['device_name'] if devices_in_stack | length > 0 else None -%}
              {%- set _ = masters.append({
                    "stack_name": stack_name,
                    "master_device": master_device
                  }) -%}
            {%- endfor -%}
            {{ masters }}
          {%- else -%}
            []
          {%- endif -%}

    - name: Update switches with stack master information (if stack exists)
      set_fact:
        switches: >-
          {%- if switches | map(attribute='is_stack') | first == true -%}
            {%- for switch in switches -%}
              {%- set master = (stack_masters | selectattr('stack_name', 'equalto', switch.stack_name) | first) -%}
              {%- set switch = switch.update({'is_stack_master': (master.master_device == switch.device_name)}) -%}
            {%- endfor -%}
          {%- endif -%}
          {{ switches }}
  
    - name: Update netbox_data with the extracted detail
      set_fact:
        netbox_data: >-
          {{
            netbox_data | combine({
              'switches': switches,
              'power_supply': power_supply,
              'fan_tray': fan_tray,
              'supervisor': supervisor,
              'linecards': linecards,
              'sfps': sfps
            })
          }}

    - name: Fail when the device details is not created
      fail:
        msg: "The device wit the details SN/VC/Priority is not defined. Current switch details {{ netbox_data['switches'] }} Stopping..."
      when: netbox_data['switches'] | length == 0
    
    - name: Create the netbox inventory role - SFP
      ansible.builtin.include_role:
        name: netbox_inventory_item_role
        tasks_from: main.yml
      vars:
        item_role: "SFP"
    
    - name: Create the netbox inventory role - POWER_SUPPLY
      ansible.builtin.include_role:
        name: netbox_inventory_item_role
        tasks_from: main.yml
      vars:
        item_role: "POWER_SUPPLY"
    
    - name: Create the netbox inventory role - FAN_TRAY
      ansible.builtin.include_role:
        name: netbox_inventory_item_role
        tasks_from: main.yml
      vars:
        item_role: "FAN_TRAY"

    - name: Create the netbox device type
      ansible.builtin.include_role:
        name: netbox_device_type
        tasks_from: main.yml
      vars:
        item_device_type: "{{ netbox_data['switches'] | map(attribute='model') | unique | first }}"
    
    - name: Create module type within NetBox for line cards
      ansible.builtin.include_role:
        name: netbox_module_type
        tasks_from: main.yml
      loop: "{{ netbox_data['linecards'] }}"
      vars:
        item_model: "{{ item['pid'] }}"
        item_part_number: "{{ item['pid'] }}"
      when: netbox_data['linecards'] | length > 0
    
    - name: Create module type within NetBox for supervisors
      ansible.builtin.include_role:
        name: netbox_module_type
        tasks_from: main.yml
      loop: "{{ netbox_data['supervisor'] }}"
      vars:
        item_model: "{{ item['pid'] }}"
        item_part_number: "{{ item['pid'] }}"
      when: netbox_data['supervisor'] | length > 0
    
    - name: Create device within NetBox with only required information
      ansible.builtin.include_role:
        name: netbox_device
        tasks_from: main.yml
      loop: "{{ netbox_data['switches'] }}"
      vars:
        item_device_name: "{{ item.device_name }}"
        item_device_type: "{{ item.model }}"
        item_device_sn: "{{ item.system_sn }}"
        item_site: "{{ site }}"
        item_device_version: "{{ item.version }}"
        item_device_role: "distribution-switch"
    
    - name: Create Power Supply
      ansible.builtin.include_role:
        name: netbox_inventory_item
        tasks_from: without_component.yml
      loop: "{{ netbox_data['power_supply'] }}"
      vars:
        item_name: "{{ item.device_name }}_psu_{{ item.power_supply_id }}"
        item_device: "{{ item.device_name }}"
        item_serial: "{{ item.sn }}"
        item_pardid: "{{ item.pid }}"
        item_manufactur: "Cisco"
        item_inventory_role: "POWER_SUPPLY"
      when: netbox_data['power_supply'] | length > 0
    
    - name: Create Fan Tray
      ansible.builtin.include_role:
        name: netbox_inventory_item
        tasks_from: without_component.yml
      loop: "{{ netbox_data['fan_tray'] }}"
      vars:
        item_name: "{{ item.device_name }}_fan_{{ item.fan_supply_id }}"
        item_device: "{{ item.device_name }}"
        item_serial: "{{ item.sn }}"
        item_pardid: "{{ item.pid }}"
        item_manufactur: "Cisco"
        item_inventory_role: "FAN_TRAY"
      when: netbox_data['fan_tray'] | length > 0
    
    - name: Create module bay within NetBox for line cards
      ansible.builtin.include_role:
        name: netbox_module_bay
        tasks_from: main.yml
      loop: "{{ netbox_data['linecards'] }}"
      vars:
        item_device: "{{ item.device_name }}"
        item_module_bay: "{{ item.device_name }}_bay_{{ item.linecards_id }}"
        item_position: "{{ item.linecards_id }}"
      when: netbox_data['linecards'] | length > 0
    
    - name: Create module type within NetBox for line cards
      ansible.builtin.include_role:
        name: netbox_module
        tasks_from: main.yml
      loop: "{{ netbox_data['linecards'] }}"
      vars:
        item_device: "{{ item.device_name }}"
        item_module_bay: "{{ item.device_name }}_bay_{{ item.linecards_id }}"
        item_module_type: "{{ item.pid }}"
        item_serial: "{{ item.sn }}"
      when: netbox_data['linecards'] | length > 0
    
    - name: Create module bay within NetBox for supervisors
      ansible.builtin.include_role:
        name: netbox_module_bay
        tasks_from: main.yml
      loop: "{{ netbox_data['supervisor'] }}"
      vars:
        item_device: "{{ item.device_name }}"
        item_module_bay: "{{ item.device_name }}_bay_{{ item.supervisor_id }}"
        item_position: "{{ item.supervisor_id }}"
      when: netbox_data['supervisor'] | length > 0
    
    - name: Create module type within NetBox for supervisors
      ansible.builtin.include_role:
        name: netbox_module
        tasks_from: main.yml
      loop: "{{ netbox_data['supervisor'] }}"
      vars:
        item_device: "{{ item.device_name }}"
        item_module_bay: "{{ item.device_name }}_bay_{{ item.supervisor_id }}"
        item_module_type: "{{ item.pid }}"
        item_serial: "{{ item.sn }}"
      when: netbox_data['supervisor'] | length > 0
    
    - name: Create interface for OOB
      ansible.builtin.include_role:
        name: netbox_device_interface
        tasks_from: main.yml
      vars:
        item_name: "{{ mgmt_vlan }}"
        item_type: "Virtual"
        item_device: >-
          {%- set stack_master = netbox_data['switches'] | selectattr('is_stack_master', 'equalto', true) | list | first %}
          {%- set standalone_device = netbox_data['switches'] | selectattr('is_stack', 'equalto', false) | list | first %}
          {%- if stack_master is defined %}
            {{ stack_master.device_name }}
          {%- elif standalone_device is defined %}
            {{ standalone_device.device_name }}
          {%- else %}
            Undefined Device
          {%- endif %}

    - name: Create IP address for OOB
      ansible.builtin.include_role:
        name: netbox_ip_address
        tasks_from: main.yml
      vars:
        item_address: "{{ ip }}"
        item_interface_name: "{{ mgmt_vlan }}"
        item_device: >-
          {%- set stack_master = netbox_data['switches'] | selectattr('is_stack_master', 'equalto', true) | list | first %}
          {%- set standalone_device = netbox_data['switches'] | selectattr('is_stack', 'equalto', false) | list | first %}
          {%- if stack_master is defined %}
            {{ stack_master.device_name }}
          {%- elif standalone_device is defined %}
            {{ standalone_device.device_name }}
          {%- else %}
            Undefined Device
          {%- endif %}
    
    - name: Update device with the OOB
      ansible.builtin.include_role:
        name: netbox_device
        tasks_from: main.yml
      loop: "{{ netbox_data['switches'] }}"
      vars:
        item_device_name: >-
          {%- set stack_master = netbox_data['switches'] | selectattr('is_stack_master', 'equalto', true) | list | first %}
          {%- set standalone_device = netbox_data['switches'] | selectattr('is_stack', 'equalto', false) | list | first %}
          {%- if stack_master is defined %}
            {{ stack_master.device_name }}
          {%- elif standalone_device is defined %}
            {{ standalone_device.device_name }}
          {%- else %}
            Undefined Device
          {%- endif %}
        item_oob_ip: "{{ ip }}"
        item_device_type: "{{ item.model }}"
        item_device_sn: "{{ item.system_sn }}"
        item_site: "{{ site }}"
        item_device_version: "{{ item.version }}"
        item_device_role: "distribution-switch"
  
    - name: Create virtual chassis within NetBox with only required information
      ansible.builtin.include_role:
        name: netbox_virtual_chassis
        tasks_from: main.yml
      vars: 
        item_vc_name: "{{ stack_masters | map(attribute='stack_name') | unique | first }}" 
        item_master: "{{ stack_masters | map(attribute='master_device') | unique | first }}" 
      when: netbox_data['switches'] | selectattr('is_stack', 'equalto', true) | list | length > 0
  
    - name: Update the device with the VC information
      ansible.builtin.include_role:
        name: netbox_device
        tasks_from: main.yml
      loop: "{{ netbox_data['switches'] }}"
      vars:
        item_device_name: "{{ item.device_name }}"
        item_vc_position: "{{ item.device_name.split('-')[-1] }}"
        item_vc_priority: "{{ item.vc_priority }}"
        item_vc_name: "{{ item.stack_name }}"
        item_device_type: "{{ item.model }}"
        item_device_sn: "{{ item.system_sn }}"
        item_site: "{{ site }}"
        item_device_version: "{{ item.version }}"
        item_device_role: "distribution-switch"
      when: netbox_data['switches'] | selectattr('is_stack', 'equalto', true) | list | length > 0
    
    - name: Create interfaces for SFPs
      ansible.builtin.include_role:
        name: netbox_device_interface
        tasks_from: main.yml
      loop: "{{ netbox_data['sfps'] }}"
      vars:
        item_device: "{{ item.device_name }}"
        item_name: "{{ item.interface }}"
        item_type: >-
          {%- if "Hundred" in item.interface -%}
          100gbase-x-qsfp28
          {%- elif "Forty" in item.interface -%}
          40gbase-x-qsfpp
          {%- elif "Tw" in item.interface -%}
          25gbase-x-sfp28
          {%- elif "Te" in item.interface -%}
          10gbase-x-sfpp
          {%- elif "Giga" in item.interface -%}
          1000base-t
          {%- elif "Fast" in item.interface -%}
          100base-tx
          {%- elif "Port" in item.interface -%}
          lag
          {%- else -%}
          virtual
          {%- endif %}
      when: netbox_data['sfps'] | length > 0

    - name: Create the inventory item - SFP
      ansible.builtin.include_role:
        name: netbox_inventory_item
        tasks_from: main.yml
      vars:
        item_name: "{{ item['device_name'] ~ '_' ~ 'sfp' ~ '_' ~ item['interface'] }}"
        item_device: "{{ item['device_name'] }}"
        item_serial: "{{ item['sn'] }}"
        item_pardid: "{{ item['pid'] }}"
        item_manufactur: "Cisco"
        item_inventory_role: "SFP"
        item_component_type: "dcim.interface"
        item_component_name: "{{ item['interface'] }}"
      loop: "{{ netbox_data['sfps'] }}"
      loop_control:
        label: "Creating SFP {{ item['device_name'] ~ '_' ~ 'sfp' ~ '_' ~ item['interface'] }} for interface {{ item['interface'] }} with SN {{ item['sn'] }}"
      when: netbox_data['sfps'] | length > 0
